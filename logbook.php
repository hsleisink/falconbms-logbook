#!/usr/bin/php
<?php
	# This is just an example script.

	require("falcon_bms_logbook.php");

	if (count($argv) <= 1) {
		printf("%s: <Falcon BMS logbook>\n", $argv[0]);
		exit;
	}
	$file = $argv[1];

	if (file_exists($file) == false) {
		printf("Falcon BMS logbook not found.\n");
		exit;
	}

	$logbook = new falcon_bms_logbook($file);

	printf("Pilot name:     %s\n", $logbook->name);
	printf("Pilot callsign: %s\n", $logbook->callsign);
	printf("Pilot rank:     %s\n", $logbook->rank);
	printf("Squadron name:  %s\n", $logbook->squadron_name);
	printf("Password:       %s\n", $logbook->password);

	$logbook->rank = 6;

	if ($logbook->save("Test.lbk") == false) {
		printf("Failed saving logbook!\n");
	} else {
		printf("Logbook saved.\n");
	}
?>
