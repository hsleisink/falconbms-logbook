<?php
	/* Falcon BMS logbook (.lbk file) library
	 */
	class falcon_bms_logbook {
		const INIT_XOR_CODE = 0x58;
		const SECRET_KEY = "Falcon is your Master";
		const PWD_MASK_1 = "Who needs a password!";
		const PWD_MASK_2 = "Repent, Falcon is coming!";
		const UNPACK_FORMAT = "Z21name/Z13callsign/Z11password/Z13commissioned/".
			"Z13options_file/C__void_0/fflight_hours/face_factor/Vrank/vdf_matches_won/".
			"vdf_matches_lost/vdf_matches_won_vs_human/vdf_matches_lost_vs_human/".
			"vdf_kills/vdf_killed/vdf_human_kills/vdf_killed_by_human/".
			"vc_games_won/vc_games_lost/vc_games_tied/vc_missions/Vc_total_score/".
			"Vc_total_mission_score/vc_consec_missions/vc_kills/vc_killed/".
			"vc_human_kills/vc_killed_by_human/vc_killed_by_self/vc_air_to_ground/".
			"vc_static/vc_naval/vc_friendlies_killed/vc_miss_since_last_friendly_kill/".
			"Z2__void_1/Cm_air_force_cross/Cm_silver_star/Cm_dist_fly_cross/".
			"Cm_air_medal/Cm_korea_campaign/Cm_longevity/Z2__void_2/Vpicture_resource/".
			"Z33picture_filename/Z2__void_3/Vpatch_resource/Z32patch_filename/Z2__void_4/".
			"Z120personal_text/C__void_5/Z16squadron_name/Z4__void_6/nvoice/Vchecksum";
		const RANK_TITLES = array("Second Lieutenant", "Lieutenant", "Captain",
			"Major", "Lieutenant Colonel", "Colonel", "Brigade General");

		private $secret_key_len = null;
		private $filename = null;
		private $logbook = null;
		private $pack_format = "";

		/* Constructor
		 */
		public function __construct($filename) {
			$this->secret_key_len = strlen(self::SECRET_KEY);

			if ($this->load($filename)) {
				$this->filename = $filename;
			}

			/* Extract pack format from unpack format
			 */
			$formats = explode("/", self::UNPACK_FORMAT);
			foreach ($formats as $format) {
				$this->pack_format .= $format[0];
				$i = 1;
				while (($format[$i] >= "0") && ($format[$i] <= "9")) {
					$this->pack_format .= $format[$i++];
				}
			}
		}

		/* Magic method get
		 */
		public function __get($key) {
			if ($this->logbook == null) {
				return null;
			}

			if (substr($key, 0, 6) == "__void") {
				return null;
			}

			switch ($key) {
				case "data":
					$logbook = $this->logbook;
					foreach ($logbook as $k => $v) {
						if (substr($k, 0, 6) == "__void") {
							unset($logbook[$k]);
						}
					}
					unset($logbook["__void_0"]);
					unset($logbook["__void_1"]);
					unset($logbook["__void_2"]);
					return $logbook;
				case "password":
					if (strlen($this->logbook["password"]) == 1) {
						if (ord($this->logbook["password"][0]) == 5) {
							return "";
						}
					}
					return $this->xor_password($this->logbook["password"]);
				case "rank":
					return self::RANK_TITLES[$this->logbook["rank"]];
			}

			if (isset($this->logbook[$key])) {
				return $this->logbook[$key];
			}

			return null;
		}

		/* Magic method set
		 */
		public function __set($key, $value) {
			if ($this->logbook == null) {
				return;
			}

			if (substr($key, 0, 6) == "__void") {
				return;
			}

			switch ($key) {
				case "checksum":
					return;
				case "password":
					$value = $this->xor_password($value);
					break;
				case "rank":
					if (is_int($value) == false) {	
						return;
					}
					if (($value < 0) || ($value > 6)) {
						return;
					}
					break;
			}

			if (isset($this->logbook[$key]) == false) {
				return;
			}

			if (gettype($this->logbook[$key]) != gettype($value)) {
				return;
			}

			$this->logbook[$key] = $value;
		}

		/* XOR a password string
		 */
		private function xor_password($password) {
			$password = str_pad($password, 11, "\0");

			$mask_1_len = strlen(self::PWD_MASK_1);
			$mask_2_len = strlen(self::PWD_MASK_2);

			for ($i = 0; $i < 10; $i++) {
				$char = $password[$i];
				$char ^= self::PWD_MASK_1[$i % $mask_1_len];
				$char ^= self::PWD_MASK_2[$i % $mask_2_len];
				$password[$i] = $char;
			}

			return $password;
		}

		/* Read and decrypt a Falcon BMS logbook
		 */
		private function load($filename) {
			/* Check logbook file status and size
			 */
			if (file_exists($filename) == false) {
				return false;
			}
			if (filesize($filename) != 372) {
				return false;
			}

			/* Read logbook file
			 */
			if (($fp = fopen($filename, "r")) == false) {
				return false;
			}
			if (($logbook = fread($fp, 372)) == false) {
				return false;
			}
			fclose($fp);

			/* Decrypt logbook
			 */
			$xor_code = chr(self::INIT_XOR_CODE);

			for ($i = 0; $i < 372; $i++) {
				$char = $logbook[$i];

				$char ^= self::SECRET_KEY[$i % $this->secret_key_len];
				$char ^= $xor_code;

				$xor_code = $logbook[$i];
				$logbook[$i] = $char;
			}

			$logbook = unpack(self::UNPACK_FORMAT, $logbook);

			if ($logbook["checksum"] != 0) {
				return false;
			}

			$this->logbook = $logbook;

			return true;
		}

		/* Encrypt and write a Falcon BMS logbook
		 */
		public function save($filename = null) {
			if ($this->logbook == null) {
				return false;
			}

			if ($filename == null) {
				$filename = $this->filename;
			}

			$args = $this->logbook;
			array_unshift($args, $this->pack_format);
			if (($logbook = call_user_func_array("pack", $args)) == false) {
				return false;
			}

			if (($len = strlen($logbook)) != 372) {
				printf("Incorrect logbook size: %d\n", $len);
				return false;
			}

			/* Encrypt logbook
			 */
			$xor_code = chr(self::INIT_XOR_CODE);

			for ($i = 0; $i < 372; $i++) {
				$char = $logbook[$i];

				$char ^= $xor_code;
				$char ^= self::SECRET_KEY[$i % $this->secret_key_len];

				$xor_code = $char;
				$logbook[$i] = $char;
			}

			/* Write logbook file
			 */
			if (($fp = fopen($filename, "w")) == false) {
				return false;
			}
			if (($logbook = fwrite($fp, $logbook)) == false) {
				return false;
			}
			fclose($fp);

			return true;
		}
	}
?>
